<?php session_start(); 

if (!isset($_SESSION['i'])){
    header("location:login.php");
    die();
}
?>

<html>
<head>
    <title>Registrar Alumnos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
	<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
    <body>
		<ul class="menu">
            <li> <a href="./info.php"  method="POST">Home</a></li>
            <li> <a href="./formulario.php"  method="POST">Registrar alumnos</a></li>
            <li> <a href="./cerrar.php" method="POST">Cerrar sesión</a></li>
        </ul>
	<div class="container-formulario">
		<div class="columns">

			<form action="./save.php" method="POST">
				<!-- form Numero de cuenta -->
				<label class="form-label" for="input-text">Número cuenta</label>
				<input name="num-cta" class="form-input " type="integer" id="num-cta" placeholder="Número de cuenta" required>
                <!-- form Nombre -->
				<label class="form-label" for="input-text">Nombre</label>
				<input name="nombre" class="form-input " type="text" id="nombre" placeholder="Nombre" required>
                <!-- form Primer Apellido -->
				<label class="form-label" for="input-text">Primer Apellido</label>
				<input name="primer_apellido" class="form-input " type="text" id="primer_apellido" placeholder="Primer Apellido">
                <!-- form Segundo Apellido -->
				<label class="form-label" for="input-text">Segundo Apellido</label>
				<input name="segundo_apellido" class="form-input " type="text" id="segundo_apellido" placeholder="Segundo Apellido">

				<!-- form radio control genero-->
				<label  class="form-label">Sexo</label>
				<label class="form-radio">
					<input type="radio" name="radio" value="H">
					<i class="form-icon"></i> Hombre
				</label>
				<label class="form-radio">
					<input type="radio" name="radio" value="M">
					<i class="form-icon"></i> Mujer
                </label>
                </label>
				<label class="form-radio">
					<input type="radio" name="radio" value="O">
					<i class="form-icon"></i> Otro
				</label>

				<!-- form date control -->
				<label class="form-label" for="input-date">Fecha de Nacimiento</label>
				<input name="date" class="form-input " type="date" id="fec_nac"
					   placeholder="fecha">

                <!-- form password control -->
				<label class="form-label" for="input-password">Contraseña</label>
				<input name="password" class="form-input" type="password" id="input-password" placeholder="Contraseña" required>   
                <br/>   
				<!-- Botones -->
				<input type='submit' class="btn" value="Registrar"/>
                <input type='reset' class="btn btn-primary" value="limpiar"/>

			</form>
		</div>
	</div>
    </body>
</html>

