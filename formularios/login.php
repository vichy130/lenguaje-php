<?php session_start(); 

if(!isset($_SESSION['usuarios'])){

    $_SESSION['usuarios']=array();
    $_SESSION['alumno']=array();
    $_SESSION['alumno']['num_cta']="1";
    $_SESSION['alumno']['nombre']='Admin';
    $_SESSION['alumno']['primer_apellido']='General';
    $_SESSION['alumno']['segundo_apellido']='';
    $_SESSION['alumno']['genero']='O';
    $_SESSION['alumno']['fec_nac']='28/02/1990';
    $_SESSION['alumno']['contrasena']='adminpass123.';
    
    array_push($_SESSION['usuarios'],$_SESSION['alumno']);
}


?>

<html>
<head>
    <title>Login Formularios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
    <body>
	<div id="container-login">
    <h2>Login</h2>
		<div class="container-php">

            
            <form action="./validar.php" method="POST">
            
				<!-- form Numero de cuenta -->
				<label class="form-label" for="input-text">Número cuenta:</label>
				<input name="login" class="form-input" type="text" id="login" placeholder="Número de cuenta" required>
                <?php //if($_SESSION['error']==true){echo "sesion invalida";} ?>
                <!-- form password control -->
				<label class="form-label" for="input-password">Contraseña:</label>
				<input name="password" class="form-input" type="password" id="input-password" placeholder="Contraseña" required>   
                <br/>   
                <!-- Boton -->
                <input type='submit' name="submit" class="btn" value="Ingresar" >

                
			</form>
		</div>
	</div>
    </body>
</html>