<?php session_start();

if (!isset($_SESSION['i'])){
    header("location:login.php");
    die();
}
?>


<html>
<head>
    <title>Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
    <body>
        <ul class="menu">
            <li> <a href="./info.php"  method="POST">Home</a></li>
            <li> <a href="./formulario.php"  method="POST">Registrar alumnos</a></li>
            <li> <a href="./cerrar.php" method="POST">Cerrar sesión</a></li>
        </ul>
	    <div class="container-info">

            <h2 id="titulo">Usuario Autenticado</h2>

            <div id="box">
                <?php
                    
                    echo $_SESSION['usuarios'][$_SESSION['i']]['nombre'];
                ?>
            </div>
            <div id="box2">
                <h3 id="subtitulo">Información</h3>
                <?php
                
                    echo "Número de cuenta: ";
                    echo $_SESSION['usuarios'][$_SESSION['i']]['num_cta'];
                    echo "<br/>";
                    echo "Fecha de nacimiento: ";
                    echo $_SESSION['usuarios'][$_SESSION['i']]['fec_nac'];
                ?>
            </div>
            <br/>
            <h3 id="titulo2">Datos guardados:</h3>
            <hr/>
                <table class="tabla">
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Fecha de Nacimiento</th>
                </tr>
                <?php 

                        $count=count($_SESSION['usuarios']);
                        
                        for($i=0; $i<$count; $i++){
                        echo "<tr><td>".$_SESSION['usuarios'][$i]['num_cta']."</td><td>".$_SESSION['usuarios'][$i]['nombre']."</td><td>".$_SESSION['usuarios'][$i]['fec_nac']."</td></tr>";
                        }

                    ?>
                </table>
	    </div>
    </body>
</html>